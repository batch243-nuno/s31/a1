// // 1. What directive is used by Node.js in loading the modules it needs?
// require() directive is used to load a Node module

// // 2. What Node.js module contains a method for server creation?
// The http module contains the function to create the server

// // 3. What is the method of the http object responsible for creating a server using Node.js?
// createServer() is used to create server

// // 4. What method of the response object allows us to set status codes and content types?
// writeHead() allows us to set status code and content type

// // 5. Where will console.log() output its contents when run in Node.js?
// terminal or in a browser

// // 6. What property of the request object contains the address's endpoint?
// .url property contains address's endpoint

const http = require("http");
const port = 3000;

const server = http.createServer((request, response) => {
  if (request.url == "/login") {
    response.writeHead(200, { "Content-Type": "text/plain" });
    response.end("Welcome login page.");

    // Homepage route
  } else {
    response.writeHead(400, { "Content-Type": "text/plain" });
    response.end("I'm sorry the page you are looking for cannot be found.");
  }
});
server.listen(port);

console.log(`Server running at localhost:${port}`);
